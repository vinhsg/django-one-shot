from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoCreate, TodoEdit, TodoCreateItem, EditItem


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todos,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoCreate(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoCreate()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoCreateItem(request.POST)
        if form.is_valid:
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoCreateItem()
    context = {"form": form}
    return render(request, "todos/createitem.html", context)


def todo_list_update(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoEdit(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail")
    else:
        form = TodoCreate(instance=post)
    context = {"todo_list_detail": post, "form": form}
    return render(request, "todos/edit.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = EditItem(request.POST, instance=post)
        if form.is_valid():
            form.save()
            return redirect("todo_list_list")
    else:
        form = EditItem(instance=post)
    context = {"todo_list_detail": post, "form": form}
    return render(request, "todos/edititem.html", context)


def todo_list_delete(request, id):
    todos = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todos.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
