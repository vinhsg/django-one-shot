from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoCreate(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoEdit(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoCreateItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = "__all__"


class EditItem(ModelForm):
    class Meta:
        model = TodoItem
        fields = "__all__"
